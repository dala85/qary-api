from rest_framework import serializers 
from .models import AskBot 




qa='qa'
faq='faq'
glossary='glossary'

bot_skills = [
	(qa,'qa'),
	(faq,'faq'),
	(glossary,'glossary')
]


class AskBotSerializers(serializers.ModelSerializer):

	class Meta:
		model = AskBot
		read_only_fields = ('id', 'answer')
		fields = ('id','question','bot','answer')

	def get_bot_skills(self,obj):
		return obj.get_bot_display()


 
