from django.contrib import admin
from django.conf.urls import url
from django.urls import path,include
from endpoints.views import AskBotViewSet
from rest_framework import routers 

router = routers.SimpleRouter()
router.register('bot',AskBotViewSet)




urlpatterns = [
	url(r'^admin/',admin.site.urls),
    path('api-auth',include('rest_framework.urls')),
]
urlpatterns += router.urls